#version 400

uniform mat4 model;
uniform mat4 view;
uniform mat4 model_view_projection;
uniform vec3 light_direction;

in vec3 position;
in vec4 color;
in vec3 normal;

out vec4 ex_color;

void main()
{
    gl_Position = model_view_projection * vec4(position, 1.);

    {
        float ambient = .15;

        vec3 normal_world_space = vec3(model * vec4(normal, 0.));
        float diffuse = clamp(dot(-normal_world_space, light_direction), 0., 1.);

        vec4 look_direction_camera_space = normalize(view * model * vec4(position, 1.));
        vec3 reflection_direction_world_space = reflect(light_direction, normal_world_space);
        vec4 reflection_direction_camera_space = view * vec4(reflection_direction_world_space, 0.);
        float cosAlpha = dot(-look_direction_camera_space, reflection_direction_camera_space);
        float roughness = 2.;
        float specular = pow(clamp(cosAlpha, 0., 1.), roughness);

        vec4 shaded_color = color;
        shaded_color.rgb *= clamp(ambient + diffuse + specular, 0., 1.);

        ex_color = shaded_color;
    }
}
