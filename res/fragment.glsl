#version 400

in vec4 ex_color;

void main()
{
    gl_FragColor = ex_color;
}
