LIBS = -lGLEW -lglfw -lGL
INCLUDES = -I/usr/include -Isrc/lib/math
FLAGS = -Wall -g -O0

debug: src/Application.cpp src/Renderer.cpp src/RubiksCube.cpp
	g++ -o build/application src/Application.cpp src/Renderer.cpp src/RubiksCube.cpp $(LIBS) $(INCLUDES) $(FLAGS)
