# pragma once

struct color
{
    float r = 0.0f;
    float g = 0.0f;
    float b = 0.0f;
    float a = 1.0f;
};

const color COLOR_BLACK  = { 0.0f, 0.0f, 0.0f, 1.0f };
const color COLOR_WHITE  = { 1.0f, 1.0f, 1.0f, 1.0f };
const color COLOR_RED    = { 1.0f, 0.0f, 0.0f, 1.0f };
const color COLOR_ORANGE = { 1.0f, 0.5f, 0.0f, 1.0f };
const color COLOR_YELLOW = { 1.0f, 1.0f, 0.0f, 1.0f };
const color COLOR_GREEN  = { 0.0f, 1.0f, 0.0f, 1.0f };
const color COLOR_BLUE   = { 0.0f, 0.0f, 1.0f, 1.0f };
const color COLOR_PURPLE = { 0.5f, 0.0f, 1.0f, 1.0f };

