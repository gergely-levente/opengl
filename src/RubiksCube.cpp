#include "RubiksCube.h"
#include "Math.h"

#include <assert.h>

using namespace math;

RubiksCube::RubiksCube()
{
    for (int z = 0, i = 0; z < 3; z++)
    {
        for (int y = 0; y < 3; y++)
        {
            for (int x = 0; x < 3; x++, i++)
            {
                color left   = x == 0 ? COLOR_ORANGE : COLOR_BLACK;
                color right  = x == 2 ? COLOR_RED    : COLOR_BLACK;
                color bottom = y == 0 ? COLOR_WHITE  : COLOR_BLACK;
                color top    = y == 2 ? COLOR_GREEN  : COLOR_BLACK;
                color back   = z == 0 ? COLOR_YELLOW : COLOR_BLACK;
                color front  = z == 2 ? COLOR_BLUE   : COLOR_BLACK;

                cubes[i] = MakeCube(front, right, left, back, top, bottom);

                float spacing = 1.1f;
                vec3 offset = { spacing, spacing, spacing };
                cubes[i].position = { (float)x, (float)y, (float)z };
                cubes[i].position *= spacing;
                cubes[i].position -= offset;

                locations[i] = i;
            }
        }
    }
}

void RubiksCube::Turn(int axis, int slice, int turns)
{
    assert(axis >= 0 && axis < 3);
    assert(slice >= 0 && slice < 3);
    // TODO: handle negative and more than 1 turns
    assert(turns == 1);

    array<int, 3*3> cubes_in_slice = GetSlice(axis, slice);
    UpdateCubeLocations(axis, slice, turns, cubes_in_slice);
    for (const int i : cubes_in_slice)
    {
        this->cubes[i].rotation = quaternion::SnapToClosestAxis(this->cubes[i].rotation);
    }
}

void RubiksCube::RotateSlice(int axis, int slice, float angle)
{
    quaternion rotation;
    if      (axis == 0) rotation = quaternion::FromEuler(angle, 0.0f, 0.0f);
    else if (axis == 1) rotation = quaternion::FromEuler(0.0f, angle, 0.0f);
    else if (axis == 2) rotation = quaternion::FromEuler(0.0f, 0.0f, angle);

    array<int, 3*3> cubes_in_slice = GetSlice(axis, slice);
    for (const int i : cubes_in_slice)
    {
        this->cubes[i].rotation = rotation * this->cubes[i].rotation;
    }
}

array<int, 3*3> RubiksCube::GetSlice(int axis, int slice) const
{
    array<int, 3*3> cubes_in_slice;

    switch (axis)
    {
        case 0:
            cubes_in_slice[0] = this->locations[slice + 3*0];
            cubes_in_slice[1] = this->locations[slice + 3*1];
            cubes_in_slice[2] = this->locations[slice + 3*2];
            cubes_in_slice[3] = this->locations[slice + 3*3];
            cubes_in_slice[4] = this->locations[slice + 3*4];
            cubes_in_slice[5] = this->locations[slice + 3*5];
            cubes_in_slice[6] = this->locations[slice + 3*6];
            cubes_in_slice[7] = this->locations[slice + 3*7];
            cubes_in_slice[8] = this->locations[slice + 3*8];
            break;
        case 1:
            cubes_in_slice[0] = this->locations[3*slice + 0];
            cubes_in_slice[1] = this->locations[3*slice + 1];
            cubes_in_slice[2] = this->locations[3*slice + 2];
            cubes_in_slice[3] = this->locations[3*slice + 9];
            cubes_in_slice[4] = this->locations[3*slice + 10];
            cubes_in_slice[5] = this->locations[3*slice + 11];
            cubes_in_slice[6] = this->locations[3*slice + 18];
            cubes_in_slice[7] = this->locations[3*slice + 19];
            cubes_in_slice[8] = this->locations[3*slice + 20];
            break;
        case 2:
            cubes_in_slice[0] = this->locations[3*3*slice + 0];
            cubes_in_slice[1] = this->locations[3*3*slice + 1];
            cubes_in_slice[2] = this->locations[3*3*slice + 2];
            cubes_in_slice[3] = this->locations[3*3*slice + 3];
            cubes_in_slice[4] = this->locations[3*3*slice + 4];
            cubes_in_slice[5] = this->locations[3*3*slice + 5];
            cubes_in_slice[6] = this->locations[3*3*slice + 6];
            cubes_in_slice[7] = this->locations[3*3*slice + 7];
            cubes_in_slice[8] = this->locations[3*3*slice + 8];
            break;
    }

    return cubes_in_slice;
}

void RubiksCube::UpdateCubeLocations(int axis, int slice, int turns, const array<int, 3*3>& cubes_in_slice)
{
    switch (axis)
    {
        case 0:
            this->locations[slice + 3*0] = cubes_in_slice[6];
            this->locations[slice + 3*1] = cubes_in_slice[3];
            this->locations[slice + 3*2] = cubes_in_slice[0];
            this->locations[slice + 3*3] = cubes_in_slice[7];
            this->locations[slice + 3*4] = cubes_in_slice[4];
            this->locations[slice + 3*5] = cubes_in_slice[1];
            this->locations[slice + 3*6] = cubes_in_slice[8];
            this->locations[slice + 3*7] = cubes_in_slice[5];
            this->locations[slice + 3*8] = cubes_in_slice[2];
            break;
        case 1:
            this->locations[3*slice + 0] = cubes_in_slice[2];
            this->locations[3*slice + 1] = cubes_in_slice[5];
            this->locations[3*slice + 2] = cubes_in_slice[8];
            this->locations[3*slice + 9] = cubes_in_slice[1];
            this->locations[3*slice + 10] = cubes_in_slice[4];
            this->locations[3*slice + 11] = cubes_in_slice[7];
            this->locations[3*slice + 18] = cubes_in_slice[0];
            this->locations[3*slice + 19] = cubes_in_slice[3];
            this->locations[3*slice + 20] = cubes_in_slice[6];
            break;
        case 2:
            this->locations[3*3*slice + 0] = cubes_in_slice[6];
            this->locations[3*3*slice + 1] = cubes_in_slice[3];
            this->locations[3*3*slice + 2] = cubes_in_slice[0];
            this->locations[3*3*slice + 3] = cubes_in_slice[7];
            this->locations[3*3*slice + 4] = cubes_in_slice[4];
            this->locations[3*3*slice + 5] = cubes_in_slice[1];
            this->locations[3*3*slice + 6] = cubes_in_slice[8];
            this->locations[3*3*slice + 7] = cubes_in_slice[5];
            this->locations[3*3*slice + 8] = cubes_in_slice[2];
            break;
    }
}

object RubiksCube::MakeCube(color front, color right, color left, color back, color top, color bottom) const
{
    object cube;

    cube.mesh.vertices = {
        { { -0.5f, -0.5f, -0.5f, }, VEC3_BACK,    back },
        { {  0.5f, -0.5f, -0.5f, }, VEC3_BACK,    back },
        { {  0.5f,  0.5f, -0.5f, }, VEC3_BACK,    back },
        { { -0.5f,  0.5f, -0.5f, }, VEC3_BACK,    back },
        { { -0.5f, -0.5f,  0.5f, }, VEC3_FORWARD, front },
        { {  0.5f, -0.5f,  0.5f, }, VEC3_FORWARD, front },
        { {  0.5f,  0.5f,  0.5f, }, VEC3_FORWARD, front },
        { { -0.5f,  0.5f,  0.5f, }, VEC3_FORWARD, front },
        { {  0.5f, -0.5f,  0.5f, }, VEC3_RIGHT,   right },
        { {  0.5f, -0.5f, -0.5f, }, VEC3_RIGHT,   right },
        { {  0.5f,  0.5f, -0.5f, }, VEC3_RIGHT,   right },
        { {  0.5f,  0.5f,  0.5f, }, VEC3_RIGHT,   right },
        { { -0.5f, -0.5f,  0.5f, }, VEC3_LEFT,    left },
        { { -0.5f, -0.5f, -0.5f, }, VEC3_LEFT,    left },
        { { -0.5f,  0.5f, -0.5f, }, VEC3_LEFT,    left },
        { { -0.5f,  0.5f,  0.5f, }, VEC3_LEFT,    left },
        { { -0.5f,  0.5f, -0.5f, }, VEC3_UP,      top },
        { {  0.5f,  0.5f, -0.5f, }, VEC3_UP,      top },
        { {  0.5f,  0.5f,  0.5f, }, VEC3_UP,      top },
        { { -0.5f,  0.5f,  0.5f, }, VEC3_UP,      top },
        { { -0.5f, -0.5f, -0.5f, }, VEC3_DOWN,    bottom },
        { {  0.5f, -0.5f, -0.5f, }, VEC3_DOWN,    bottom },
        { {  0.5f, -0.5f,  0.5f, }, VEC3_DOWN,    bottom },
        { { -0.5f, -0.5f,  0.5f, }, VEC3_DOWN,    bottom },
    };

    cube.mesh.indices = {
        // back face
        1, 0, 2,
        3, 2, 0,
        // front face
        4, 5, 6,
        6, 7, 4,
        // right face
        8, 9, 10,
        10, 11, 8,
        // left face
        13, 12, 14,
        15, 14, 12,
        // top face
        17, 16, 18,
        19, 18, 16,
        // bottom face
        20, 21, 22,
        22, 23, 20,
    };

    return cube;
}

