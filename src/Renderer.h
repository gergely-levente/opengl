#pragma once

#include "Math.h"
#include "Color.h"

#define GLEW_STATIC
#include <GL/glew.h>

#include <iostream>
#include <vector>

using namespace std;
using namespace math;

struct vertex
{
    vec3 position;
    vec3 normal;
    ::color color;
};

struct mesh
{
    vector<vertex> vertices;
    vector<unsigned int> indices;
};

struct object
{
    ::mesh mesh;
    vec3 position;
    quaternion rotation;
    vec3 scale = VEC3_ONE;
};

class Renderer
{
    public:
        mat4 view_matrix;
        mat4 projection_matrix;
        vec3 light_direction;

    private:
        GLuint shader_program = 0;
        GLuint vertex_shader = 0;
        GLuint fragment_shader = 0;

        GLint mvp_uniform_location = -1;
        GLint model_uniform_location = -1;
        GLint view_uniform_location = -1;
        GLint light_direction_uniform_location = -1;

    public:
        Renderer();
        ~Renderer();
        void Draw(const vector<object>& entities) const;
        void LoadShaders();

    private:
        void Draw(const object& entity) const;
        GLuint CreateShader(GLenum shader_type, const string& file_path) const;
};
