#include "Renderer.h"

#include <fstream>

using namespace std;
using namespace math;

void GLAPIENTRY OnOpenGLMessage(GLenum source, GLenum type, GLuint id, GLenum severity,
        GLsizei length, const GLchar* message, const void* userParam);

Renderer::Renderer()
{
    if (glewInit() != GLEW_OK)
    {
        cout << "Cannot initialize glew" << endl;
    }

    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(OnOpenGLMessage, 0);

    cout << "Renderer: " << glGetString(GL_RENDERER) << endl;
    cout << "OpenGL version: " << glGetString(GL_VERSION) << endl;


    this->shader_program = glCreateProgram();
    LoadShaders();
    glUseProgram(this->shader_program);

    this->mvp_uniform_location = glGetUniformLocation(this->shader_program, "model_view_projection");
    this->model_uniform_location = glGetUniformLocation(this->shader_program, "model");
    this->view_uniform_location = glGetUniformLocation(this->shader_program, "view");
    this->light_direction_uniform_location = glGetUniformLocation(this->shader_program, "light_direction");

    {
        GLuint vertex_array;
        glGenVertexArrays(1, &vertex_array);
        glBindVertexArray(vertex_array);
    }

    {
        GLuint vertex_buffer;
        glGenBuffers(1, &vertex_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
        GLuint vertex_buffer_binding_point = 0;
        glBindVertexBuffer(vertex_buffer_binding_point, vertex_buffer, 0, sizeof(vertex));

        GLint vert_position_attrib_location = glGetAttribLocation(this->shader_program, "position");
        glEnableVertexAttribArray(vert_position_attrib_location);
        int position_offset = offsetof(vertex, position);
        glVertexAttribFormat(vert_position_attrib_location, 3, GL_FLOAT, GL_FALSE, position_offset);
        glVertexAttribBinding(vert_position_attrib_location, vertex_buffer_binding_point);

        GLint vert_color_attrib_location = glGetAttribLocation(this->shader_program, "color");
        glEnableVertexAttribArray(vert_color_attrib_location);
        int color_offset = offsetof(vertex, color);
        glVertexAttribFormat(vert_color_attrib_location, 4, GL_FLOAT, GL_FALSE, color_offset);
        glVertexAttribBinding(vert_color_attrib_location, vertex_buffer_binding_point);

        GLint vert_normal_attrib_location = glGetAttribLocation(this->shader_program, "normal");
        glEnableVertexAttribArray(vert_normal_attrib_location);
        int normal_offset = offsetof(vertex, normal);
        glVertexAttribFormat(vert_normal_attrib_location, 3, GL_FLOAT, GL_FALSE, normal_offset);
        glVertexAttribBinding(vert_normal_attrib_location, vertex_buffer_binding_point);
    }

    {
        GLuint index_buffer;
        glGenBuffers(1, &index_buffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
    }

    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

    // TODO: handle initialization errors
}

Renderer::~Renderer()
{
    // TODO: clean up
}

void Renderer::Draw(const vector<object>& entities) const
{
    glUniformMatrix4fv(this->view_uniform_location, 1, GL_FALSE, this->view_matrix.elements);
    const float* light_direction = reinterpret_cast<const float*>(&this->light_direction);
    glUniform3fv(this->light_direction_uniform_location, 1, light_direction);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (const object& entity : entities)
    {
        Draw(entity);
    }
}

void Renderer::Draw(const object& entity) const
{
    mat4 model = quaternion::ToMat4(entity.rotation)
               * mat4::Translation(entity.position)
               * mat4::Scale(entity.scale);

    mat4 model_view_projection = this->projection_matrix * this->view_matrix * model;

    glUniformMatrix4fv(this->model_uniform_location, 1, GL_FALSE, model.elements);
    glUniformMatrix4fv(this->mvp_uniform_location, 1, GL_FALSE, model_view_projection.elements);

    int vertex_buffer_size = entity.mesh.vertices.size() * sizeof(vertex);
    glBufferData(GL_ARRAY_BUFFER, vertex_buffer_size, entity.mesh.vertices.data(), GL_DYNAMIC_DRAW);

    int index_buffer_size = entity.mesh.indices.size() * sizeof(GLuint);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, index_buffer_size, entity.mesh.indices.data(), GL_DYNAMIC_DRAW);

    glDrawElements(GL_TRIANGLES, entity.mesh.indices.size(), GL_UNSIGNED_INT, NULL);
}

void Renderer::LoadShaders()
{
    if (vertex_shader != 0)
    {
        glDetachShader(this->shader_program, this->vertex_shader);
        glDeleteShader(this->vertex_shader);
    }
    if (fragment_shader != 0)
    {
        glDetachShader(this->shader_program, this->fragment_shader);
        glDeleteShader(this->fragment_shader);
    }

    this->vertex_shader = CreateShader(GL_VERTEX_SHADER, "res/vertex.glsl");
    this->fragment_shader = CreateShader(GL_FRAGMENT_SHADER, "res/fragment.glsl");

    glAttachShader(this->shader_program, this->vertex_shader);
    glAttachShader(this->shader_program, this->fragment_shader);
    glLinkProgram(this->shader_program);
    glUseProgram(this->shader_program);
}

GLuint Renderer::CreateShader(GLenum shader_type, const string& file_path) const
{
    ifstream shader_file(file_path); 
    string shader_source;
    if (shader_file.is_open())
    {
        string line;
        while (getline(shader_file, line))
        {
            shader_source += line + "\n";
        }
    }

    GLuint shader = glCreateShader(shader_type);
    const char* s = shader_source.c_str();
    glShaderSource(shader, 1, &s, NULL);
    glCompileShader(shader);

    return shader;
}

void GLAPIENTRY OnOpenGLMessage(GLenum source, GLenum type, GLuint id, GLenum severity,
        GLsizei length, const GLchar* message, const void* userParam)
{
    // TODO: decode error messages

    /* #define GL_DEBUG_SEVERITY_NOTIFICATION    0x826B */
    /* #define GL_DEBUG_SEVERITY_HIGH            0x9146 */
    /* #define GL_DEBUG_SEVERITY_MEDIUM          0x9147 */
    /* #define GL_DEBUG_SEVERITY_LOW             0x9148 */

    /* #define GL_DEBUG_SOURCE_API               0x8246 */
    /* #define GL_DEBUG_SOURCE_WINDOW_SYSTEM     0x8247 */
    /* #define GL_DEBUG_SOURCE_SHADER_COMPILER   0x8248 */
    /* #define GL_DEBUG_SOURCE_THIRD_PARTY       0x8249 */
    /* #define GL_DEBUG_SOURCE_APPLICATION       0x824A */
    /* #define GL_DEBUG_SOURCE_OTHER             0x824B */

    /* #define GL_DEBUG_TYPE_ERROR               0x824C */
    /* #define GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR 0x824D */
    /* #define GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR  0x824E */
    /* #define GL_DEBUG_TYPE_PORTABILITY         0x824F */
    /* #define GL_DEBUG_TYPE_PERFORMANCE         0x8250 */
    /* #define GL_DEBUG_TYPE_OTHER               0x8251 */

    string type_string;
    switch(type)
    {
        case GL_DEBUG_TYPE_ERROR:
            type_string = "ERROR";
            break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            type_string = "DEPRECATED BEHAVIOR";
            break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            type_string = "UNDEFINED BEHAVIOR";
            break;
        case GL_DEBUG_TYPE_PORTABILITY:
            type_string = "PORTABILITY";
            break;
        case GL_DEBUG_TYPE_PERFORMANCE:
            type_string = "PERFORMANCE";
            break;
        case GL_DEBUG_TYPE_MARKER:
            type_string = "MARKER";
            break;
        case GL_DEBUG_TYPE_PUSH_GROUP:
            type_string = "PUSH GOUP";
            break;
        case GL_DEBUG_TYPE_POP_GROUP:
            type_string = "POP_GOUP";
            break;
        case GL_DEBUG_TYPE_OTHER:
            type_string = "OTHER";
            break;
        default:
            type_string = "unhandled error type";
    }

    if (severity != GL_DEBUG_SEVERITY_NOTIFICATION)
    {
        cout << "OpenGL " << type_string
            << " severity:" << hex <<  severity
            << " message: " << message << endl;
    }
}
