#include "RubiksCube.h"
#include "Math.h"
// NOTE: renderer incudes glew, which must be included before glfw
#include "Renderer.h"

#include <GLFW/glfw3.h>

#include <iostream>

using namespace std;
using namespace math;

struct camera
{
    float fov_y;
    float aspect_ratio;
    float near_clip;
    float far_clip;

    vec3 position;
};

void OnKey(GLFWwindow* window, int key, int scancode, int action, int mods);

int main(void)
{
    if (!glfwInit())
    {
        cout << "Cannot initialize glfw" << endl;
        return -1;
    }

    int resolution_x = 640;
    int resolution_y = 480;
    GLFWwindow* window = glfwCreateWindow(resolution_x, resolution_y, "Hello World", NULL, NULL);
    if (!window)
    {
        cout << "Cannot create window" << endl;
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, OnKey);

    ::camera camera;
    camera.fov_y = 60.0_deg;
    camera.aspect_ratio = (float)resolution_x / (float)resolution_y;
    camera.near_clip = 0.1f;
    camera.far_clip = 100.0f;
    camera.position = { 4.0f, 3.0f, 6.0f };

    Renderer renderer;
    renderer.light_direction = Normalize({ -1.0f, -2.0f, -3.0f });
    renderer.projection_matrix = mat4::Projection(camera.fov_y, camera.aspect_ratio, camera.near_clip, camera.far_clip);

    RubiksCube rubik;
    int axis = 0;
    int slice = 2;
    int turns = 1;

    double last_time = glfwGetTime();
    double last_shader_reload_time = glfwGetTime();
    double last_turn_time = glfwGetTime();
    while (!glfwWindowShouldClose(window))
    {
        double now = glfwGetTime();
        double delta_time = now - last_time;
        last_time = now;

        double shader_reload_interval = 1.0;
        if (now - last_shader_reload_time > shader_reload_interval)
        {
            last_shader_reload_time = now;
            renderer.LoadShaders();
        }

        double turn_interval = 0.5;
        if (now - last_turn_time > turn_interval)
        {
            last_turn_time = now;

            rubik.Turn(axis, slice, turns);

            axis = (rand()) % 3;
            slice = (rand()) % 3;
        }
        else
        {
            float angle_delta = (0.5f * PI) * turns / turn_interval * delta_time;
            rubik.RotateSlice(axis, slice, angle_delta);
        }

        float camera_distance = 8.0f;
        float camera_angle = 2.0f * PI * 0.2f * now;
        camera.position.x = camera_distance * cos(camera_angle);
        camera.position.z = camera_distance * sin(camera_angle);
        renderer.view_matrix = mat4::LookAt(camera.position, VEC3_ZERO);

        vector<object> entities(rubik.cubes.begin(), rubik.cubes.end());
        renderer.Draw(entities);

        glfwSwapBuffers(window);

        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}

void OnKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
    {
        if (key == GLFW_KEY_ENTER || key == GLFW_KEY_ESCAPE)
        {
            glfwSetWindowShouldClose(window, GLFW_TRUE);
        }
    }
}

