#pragma once

#include "Angle.h"
#include "Vec3.h"
#include "Mat4.h"
#include "Quaternion.h"

#include <math.h>

namespace math
{

    inline float MoveTowards(float current, float target, float delta)
    {
        float result;
        if (abs(current - target) <= delta)
        {
            result = target;
        }
        else
        {
            result = current < target ?
                current + delta :
                current - delta;
        }

        return result;
    }

}

