#pragma once

#include "Angle.h"

namespace math
{

    struct mat4
    {
        float elements[4*4] = {
            0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,
        };

        mat4 operator*=(const mat4& r)
        {
            mat4 l = *this;
            for (int row = 0; row < 4; row++)
            {
                for (int column = 0; column < 4; column++)
                {
                    float sum = 0.0f;
                    for (int offset = 0; offset < 4; offset++)
                    {
                        sum += l.elements[4*offset + column] * r.elements[4*row + offset];
                    }
                    this->elements[4*row + column] = sum;
                }
            }
            return *this;
        }

        static mat4 Translation(const vec3& translation);
        static mat4 Scale(const vec3& scale);
        static mat4 Projection(float fov_y_degrees, float aspect_ratio, float near_clip, float far_clip);
        static mat4 LookAt(const vec3& from, const vec3& to);
    };

    inline mat4 operator*(const mat4& l, const mat4& r)
    {
        mat4 result = l;
        result *= r;
        return result;
    }

    inline mat4 mat4::Translation(const vec3& translation)
    {
        return {
            1.0f,          0.0f,          0.0f,          0.0f,
            0.0f,          1.0f,          0.0f,          0.0f,
            0.0f,          0.0f,          1.0f,          0.0f,
            translation.x, translation.y, translation.z, 1.0f
        };
    }

    inline mat4 mat4::Scale(const vec3& scale)
    {
        return {
            scale.x, 0.0f,    0.0f,    0.0f,
            0.0f,    scale.y, 0.0f,    0.0f,
            0.0f,    0.0f,    scale.z, 0.0f,
            0.0f,    0.0f,    0.0f,    1.0f,
        };
    }

    inline mat4 mat4::Projection(float fov_y, float aspect_ratio, float near, float far)
    {
        float top = near * tan(0.5f * fov_y);
        float right = aspect_ratio * top;

        return {
            near/right, 0.0f,     0.0f,                      0.0f,
            0.0f,       near/top, 0.0f,                      0.0f,
            0.0f,       0.0f,     -(far+near)/(far-near),    -1.0f,
            0.0f,       0.0f,     -2.0f*far*near/(far-near), 0.0f
        };

    }

    inline mat4 mat4::LookAt(const vec3& from, const vec3& to)
    {
        vec3 camera_forward = Normalize(from - to);
        vec3 camera_right = Normalize(Cross(VEC3_UP, camera_forward));
        vec3 camera_up = Cross(camera_forward, camera_right);

        mat4 view_orientation = {
            camera_right.x, camera_up.x, camera_forward.x, 0.0f,
            camera_right.y, camera_up.y, camera_forward.y, 0.0f,
            camera_right.z, camera_up.z, camera_forward.z, 0.0f,
            0.0f,           0.0f,        0.0f,             1.0f,
        };
        mat4 view_translation = mat4::Translation(-from);

        return  view_orientation * view_translation;
    }

    const mat4 MAT4_IDENTITY = {
        1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f,
    };

}

