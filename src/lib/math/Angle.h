#pragma once

#include <math.h>

namespace math
{

    constexpr float PI = 3.14159265358979323846264338327950288419716939937510f;

    inline constexpr float operator"" _deg(long double degrees)
    {
        return static_cast<float>(degrees) / 180.0f * PI;
    }

    inline constexpr float operator"" _rad(long double radians)
    {
        return static_cast<float>(radians);
    }

}

