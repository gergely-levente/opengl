#pragma once

#include <math.h>

namespace math
{

    struct vec3
    {
        float x = 0.0f;
        float y = 0.0f;
        float z = 0.0f;

        vec3 operator+=(const vec3& v)
        {
            x += v.x;
            y += v.y;
            z += v.z;
            return *this;
        }

        vec3 operator-=(const vec3& v)
        {
            x -= v.x;
            y -= v.y;
            z -= v.z;
            return *this;
        }

        vec3 operator*=(float scalar)
        {
            x *= scalar;
            y *= scalar;
            z *= scalar;
            return *this;
        }

        vec3 operator/=(float scalar)
        {
            x /= scalar;
            y /= scalar;
            z /= scalar;
            return *this;
        }
    };

    inline vec3 operator+(const vec3& l, const vec3& r)
    {
        vec3 result = l;
        result += r;
        return result;
    }

    inline vec3 operator-(const vec3& l, const vec3& r)
    {
        vec3 result = l;
        result -= r;
        return result;
    }

    inline vec3 operator-(const vec3& v)
    {
        return { -v.x, -v.y, -v.z };
    }

    inline vec3 operator*(float scalar, const vec3& v)
    {
        vec3 result = v;
        result *= scalar;
        return result;
    }

    inline vec3 operator*(const vec3& v, float scalar)
    {
        return scalar * v;
    }

    inline vec3 operator/(const vec3& v, float scalar)
    {
        vec3 result = v;
        result /= scalar;
        return result;
    }

    inline float LengthSquared(const vec3& v)
    {
        return v.x * v.x + v.y * v.y + v.z * v.z;
    }

    inline float Length(const vec3& v)
    {
        return sqrt(LengthSquared(v));
    }

    inline vec3 Normalize(const vec3& v)
    {
        return v / Length(v);
    }

    inline vec3 Cross(const vec3& l, const vec3& r)
    {
        return {
            l.y * r.z - l.z * r.y,
            l.z * r.x - l.x * r.z,
            l.x * r.y - l.y * r.x,
        };
    }

    const vec3 VEC3_ZERO    = {  0.0f,  0.0f,  0.0f };
    const vec3 VEC3_ONE     = {  1.0f,  1.0f,  1.0f };

    const vec3 VEC3_RIGHT   = {  1.0f,  0.0f,  0.0f };
    const vec3 VEC3_LEFT    = { -1.0f,  0.0f,  0.0f };
    const vec3 VEC3_UP      = {  0.0f,  1.0f,  0.0f };
    const vec3 VEC3_DOWN    = {  0.0f, -1.0f,  0.0f };
    const vec3 VEC3_FORWARD = {  0.0f,  0.0f,  1.0f };
    const vec3 VEC3_BACK    = {  0.0f,  0.0f, -1.0f };

}

