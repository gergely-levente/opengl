#pragma once

#include "Vec3.h"
#include "Mat4.h"

#include <assert.h>

namespace math
{
    // https://youtu.be/jlskQDR8-bY
    // http://www.weizmann.ac.il/sci-tea/benari/sites/sci-tea.benari/files/uploads/softwareAndLearningMaterials/quaternion-tutorial-2-0-1.pdf
    // http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/Quaternions.pdf
    // http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/index.htm

    struct quaternion
    {
        float x = 0.0f;
        float y = 0.0f;
        float z = 0.0f;
        float w = 1.0f;

        quaternion operator*=(const quaternion& r)
        {
            quaternion l = *this;

            this->x = l.w*r.x + l.x*r.w + l.y*r.z - l.z*r.y;
            this->y = l.w*r.y - l.x*r.z + l.y*r.w + l.z*r.x;
            this->z = l.w*r.z + l.x*r.y - l.y*r.x + l.z*r.w;
            this->w = l.w*r.w - l.x*r.x - l.y*r.y - l.z*r.z;

            return *this;
        }

        static mat4 ToMat4(const quaternion& q);
        static quaternion FromEuler(float x, float y, float z);
        static quaternion FromEuler(const vec3& v);
        static vec3 ToEuler(const quaternion& q);
        static quaternion SnapToClosestAxis(const quaternion& q);

    };

    inline quaternion operator*(const quaternion& l, const quaternion& r)
    {
        quaternion result = l;
        result *= r;
        return result;
    }

    inline mat4 quaternion::ToMat4(const quaternion& q)
    {
        float q0 = q.w;
        // NOTE: rotation directions are reversed to match mathematical positive direction
        float q1 = -q.x;
        float q2 = -q.y;
        float q3 = -q.z;

        mat4 result;

        result.elements[4*0 + 0] = 2.0f*(q0*q0 + q1*q1) - 1.0f;
        result.elements[4*0 + 1] = 2.0f*(q1*q2 - q0*q3);
        result.elements[4*0 + 2] = 2.0f*(q0*q2 + q1*q3);

        result.elements[4*1 + 0] = 2.0f*(q0*q3 + q1*q2);
        result.elements[4*1 + 1] = 2.0f*(q0*q0 + q2*q2) - 1.0f;
        result.elements[4*1 + 2] = 2.0f*(q2*q3 - q0*q1);

        result.elements[4*2 + 0] = 2.0f*(q1*q3 - q0*q2);
        result.elements[4*2 + 1] = 2.0f*(q0*q1 + q2*q3);
        result.elements[4*2 + 2] = 2.0f*(q0*q0 + q3*q3) - 1.0f;

        result.elements[4*3 + 3] = 1.0f;

        return result;
    }

    inline quaternion quaternion::FromEuler(float x, float y, float z)
    {
        quaternion rotation_x = { sin(x / 2.0f), 0.0f, 0.0f, cos(x / 2.0f) };
        quaternion rotation_y = { 0.0f, sin(y / 2.0f), 0.0f, cos(y / 2.0f) };
        quaternion rotation_z = { 0.0f, 0.0f, sin(z / 2.0f), cos(z / 2.0f) };

        // y = yaw, x = pitch, z = roll
        return rotation_z * rotation_x * rotation_y;
    }

    inline quaternion quaternion::FromEuler(const vec3& v)
    {
        return FromEuler(v.x, v.y, v.z);
    }

    inline vec3 quaternion::ToEuler(const quaternion& q)
    {
        // Note: Although p0 is the scalar component of the quaternion, and p1, p2, p3  are the remaining components, the subscripts indicate the order of rotation. So for example, if the rotation is aroundthe y-axis, then around the x-axis, then around the z-axis, the quaternion is ( p0, p2, p1, p3), not (p0, p1, p2, p3)! [http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/Quaternions.pdf]
        // IMPORTANT: rotation order must match in FromEuler and ToEuler!
        float q0 = q.w;
        float q1 = q.y;
        float q2 = q.x;
        float q3 = q.z;

        float yaw, pitch, roll;

        float arg_pitch = 2.0f*(q0*q2 + q3*q1);
        float singularity_treshold = 0.999f; // ~2.5°
        // singularity @ pitch = +- 90°
        if (arg_pitch > singularity_treshold)
        {
            yaw = 2.0f * atan2(q1, q0);
            pitch = PI / 2.0f;
            roll = 0.0f;
        }
        else if (arg_pitch < -singularity_treshold)
        {
            yaw = 2.0f * atan2(q1, q0);
            pitch = -PI / 2.0f;
            roll = 0.0f;
        }
        else
        {
            yaw = atan2(2.0f*(q0*q1 - q2*q3), 1.0f-2.0f*(q1*q1 + q2*q2));
            pitch = asin(arg_pitch);
            roll = atan2(2.0f*(q0*q3 - q1*q2), 1.0f-2.0f*(q2*q2 + q3*q3));
        }

        {
            // check if any of the rotation components are NaN
            // https://stackoverflow.com/questions/570669/checking-if-a-double-or-float-is-nan-in-c#570694
            assert(pitch == pitch);
            assert(yaw == yaw);
            assert(roll == roll);
        }

        vec3 result = { pitch, yaw, roll };
        return result;
    }

    inline quaternion quaternion::SnapToClosestAxis(const quaternion& q)
    {
        vec3 euler = quaternion::ToEuler(q);
        vec3 snapped = {
            round(euler.x / (0.5f * PI)) * (0.5f * PI),
            round(euler.y / (0.5f * PI)) * (0.5f * PI),
            round(euler.z / (0.5f * PI)) * (0.5f * PI),
        };

        quaternion result = quaternion::FromEuler(snapped);
        return result;
    }

}

