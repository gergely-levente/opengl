#pragma once

#include "Renderer.h"
#include "Color.h"

#include <array>

class RubiksCube
{
    public:
        array<object, 3*3*3> cubes;

    private:
        array<int, 3*3*3> locations;

    public:
        RubiksCube();
        void Turn(int axis, int slice, int amount);
        void RotateSlice(int axis, int slice, float angle);

    private:
        object MakeCube(color front, color right, color left, color back, color top, color bottom) const;
        array<int, 3*3> GetSlice(int axis, int slice) const;
        void UpdateCubeLocations(int axis, int slice, int turns, const array<int, 3*3>& cubes_in_slice);
};

