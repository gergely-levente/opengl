nnoremap <F5> :wa<CR> :make!<CR>
nnoremap <F6> :wa<CR> :!primusrun ./build/application<CR>
nnoremap <F7> :wa<CR> :make!<CR> :!primusrun ./build/application<CR>
nnoremap <F8> :wa<CR> :!gdbgui ./build/application<CR>
